#!/usr/bin/python
# -*- coding: UTF-8 -*-
import argparse
import sys

from code_metric.terminal import launch_stats_for_commit, launch_stats_for_version, launch_stats_for_repo, \
    launch_stats_for_file, launch_stats_for_org


def main():
    parser = argparse.ArgumentParser(description='代码度量命令行工具')

    subparsers = parser.add_subparsers(help='代码度量对象')

    commit_parser = subparsers.add_parser(name='commit', help='统计commit')
    commit_parser.add_argument('-o', '--org', help="组织名称", default='openharmony')
    commit_parser.add_argument('-r', '--repo', required=True, help="仓库名称")
    commit_parser.add_argument('-s', '--sha', nargs='+', help="commit哈希")
    commit_parser.set_defaults(func=launch_stats_for_commit)

    version_parser = subparsers.add_parser(name='version', help='统计版本')
    version_parser.add_argument('-o', '--org', help="组织名称", metavar='', default='openharmony')
    version_parser.add_argument('-v', '--ver',  nargs='+', metavar='', help="版本名称")
    version_parser.add_argument('-f', '--filter', type=bool, metavar='', help="过滤三方")
    stat_type = version_parser.add_mutually_exclusive_group()
    stat_type.add_argument('--cloc', action='store_true', help='统计维度：项目代码量')
    stat_type.add_argument('--blame', action='store_true', help='统计维度：贡献者代码量')
    stat_type.add_argument('--group', action='store_true', help='统计维度：各方贡献代码量')
    version_parser.set_defaults(func=launch_stats_for_version)

    repo_parser = subparsers.add_parser(name='repo', help='统计仓库')
    repo_parser.add_argument('-o', '--org', help="组织名称", default='openharmony')
    repo_parser.add_argument('-r', '--repo',  nargs='+', help="仓库名称")
    repo_parser.add_argument('-b', '--ref', help="仓库索引名称", default='master')
    repo_parser.add_argument('-t', '--type', help="仓库索引类型", default='Branch')
    stat_type = repo_parser.add_mutually_exclusive_group()
    stat_type.add_argument('--cloc', action='store_true', help='统计维度：项目代码量')
    stat_type.add_argument('--blame', action='store_true', help='统计维度：贡献者代码量')
    repo_parser.set_defaults(func=launch_stats_for_repo)

    file_parser = subparsers.add_parser(name='file', help='统计文件')
    file_parser.add_argument('-o', '--org', help="组织名称", default='openharmony')
    file_parser.add_argument('-r', '--repo', required=True, help="仓库名称")
    file_parser.add_argument('-b', '--ref', help="仓库索引名称", default='master')
    file_parser.add_argument('-t', '--type', help="仓库索引类型(Branch/Tag/SHA)", default='Branch')
    file_parser.add_argument('-p', '--path',  nargs='+', help="文件相对地址")
    file_parser.set_defaults(func=launch_stats_for_file)

    org_parser = subparsers.add_parser(name='org', help='统计组织')
    org_parser.add_argument('-o', '--org', help="组织名称", metavar='')
    org_parser.add_argument('-f', '--filter', type=bool, metavar='', help="过滤三方")
    stat_type = org_parser.add_mutually_exclusive_group()
    stat_type.add_argument('--cloc', action='store_true', help='统计维度：项目代码量')
    stat_type.add_argument('--blame', action='store_true', help='统计维度：贡献者代码量')
    org_parser.set_defaults(func=launch_stats_for_org)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
