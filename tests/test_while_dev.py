import os.path
import unittest

from code_metric.config import init_global_config_setting, get_org_dynamic_config
from code_metric.codes import Organization, Repo, Version, Contribution
from code_metric.handler import OHVersionInfoHandler
from code_metric.utils_str import find_author_email_in_blame, parse_email_to_host

CONFIG = init_global_config_setting()


class CodeCountTestCase(unittest.TestCase):
    def test_is_config_info_pass_well(self):
        """ 判断config.py是否有效传递config.toml配置信息给全局 """
        self.assertTrue('access_token' in CONFIG)

    def test_is_dynamic_config_info_pass_well(self):
        version_info_dict = get_org_dynamic_config('openharmony')
        self.assertEqual(version_info_dict['master']['time'], 'now')

    def test_is_email_regex_works(self):
        line = '9f51ea (<aa.xxx@huawei.com> 2023-06-01 18:48:31 +0800  1) # Copyright (c) 2020 Huawei Device Co., Ltd.'
        email = find_author_email_in_blame(line)
        self.assertEqual(email, 'aa.xxx@huawei.com')
        host = parse_email_to_host(email)
        self.assertEqual(host, '@huawei.com')

    # def test_is_organization_singleton(self):
    #     org_name = 'openharmony'
    #     org1 = Organization(org_name)
    #     org2 = Organization(org_name)
    #     self.assertTrue(org1 is org2)

    def test_is_organization_repo_dict_right_class(self):
        org_name = 'openharmony'
        org = Organization(org_name)
        repo_dict = org.get_repo_object_list()
        self.assertTrue(isinstance(repo_dict[0], Repo))

    def test_is_default_version_handler_works_expected(self):
        version = Version('openharmony-retired')
        repo_url_list = version.get_repo_url_list()
        self.assertTrue(len(repo_url_list) >= 64)
        self.assertTrue(os.path.isfile(version.version_repos_info_file))

    def test_is_oh_version_handler_get_right_repo_list(self):
        handler = OHVersionInfoHandler(version_name='OpenHarmony-1.0')
        version = Version('openharmony', version_name='OpenHarmony-1.0', info_handler=handler)
        repo_url_list = version.get_repo_url_list()
        self.assertTrue(isinstance(repo_url_list, list))
        self.assertEqual(len(repo_url_list), 129)

    def test_is_version_get_repo_obj_right(self):
        version = Version('openharmony-retired')
        repo_obj_list = version.get_repo_object_list()
        self.assertTrue(len(repo_obj_list) >= 64)
        self.assertTrue(isinstance(repo_obj_list[0], Repo))

        version = Version('openharmony', 'OpenHarmony-1.0', info_handler=OHVersionInfoHandler('OpenHarmony-1.0'))
        repo_obj_list = version.get_repo_object_list()
        self.assertTrue(len(repo_obj_list) == 129)

    def test_is_author_company_right(self):
        coder = Contribution(None, 'test@huawei.com', 1024)
        self.assertEqual(coder.get_email_host(), '@huawei.com')
        self.assertEqual(coder.get_author_company(), '华为')

        coder = Contribution(None, 'no_email_record', 1024)
        self.assertEqual(coder.get_email_host(), CONFIG['no_host'])
        self.assertEqual(coder.get_author_company(), '未知贡献者')
