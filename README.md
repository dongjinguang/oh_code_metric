# 

<!-- markdownlint-disable MD033 MD041-->

<p align="center">
  <h1 align="center">OH-Code-Metric</h1>
</p>

<!-- markdownlint-disable MD033 MD041-->


### 介绍

在开源社区贡献代码时，开发者可以直观看到本次提交代码贡献量。但如果需要知道组织下各仓库总代码存量、某仓库各贡献者代码存量、某文件的贡献者及代码量等，就没有那么直观了。`OH-Code-Metric`可以用于度量组织、仓库、文件与具体commit的代码存量与贡献者信息，帮助社区开发者更直观的看到自己为开源社区做出伟大贡献。



### 环境准备

- 安装配置git命令行工具，并获取脚本代码

  1. 有关Git安装的信息，请访问[Git (git-scm.com)](https://git-scm.com/)获取。

  2. 克隆或下载脚本代码，请访问[https://gitee.com/rtos_yuan/oh_code_metric](https://gitee.com/rtos_yuan/oh_code_metric)进行克隆或者下载。

- 安装Python和依赖的Python三方包

  1. 有关Python安装的信息，请访问[https://www.python.org/](https://www.python.org/)获取。

  2. 安装工具依赖的Python三方包。

     如果没有安装，在`OH-Code-Metric`目录，进入命令行终端，通过执行如下命令，来安装依赖的三方包：

     ```shell
     pip install -r requirements.txt
     ```

     该过程只需要执行一次，如果已经安装依赖的三方包，则不需要执行。

     > 另外， 还可以直接执行如下命令，进行安全安装：
     >
     > ```shell
     > pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple requests
     > pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple pandas
     > pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple toml
     > pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple xToolkit
     > pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple xlrd
     > pip install --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple XlsxWriter
     > ```

- 配置token信息于工具根目录下的`config.toml`文件 

  访问[Gitee OpenAPI](https://gitee.com/api/v5/swagger)，点击右上角的申请授权按钮申请。

  把申请到的`access_token`替代文件中的"Please replace this with your own token"，需要保留双引号。

  ```toml
  gitee_token = "Please replace this with your own token"
  ```



### 使用说明

工具支持多种角度，对代码存量进行度量。

#### 度量organization中代码贡献量

```shell
python stats.py org -o <organization_name> [--cloc | --blame]
```

>   使用示例
>
> 从项目代码存量维度，进行度量
>
> ```shell
> python stats.py org -o openharmony-retired --cloc
> ```
>
> 从贡献者代码量维度，进行度量
>
> ```shell
> python stats.py org -o openharmony-retired --blame
> ```
>
> - 如果组织是openharmony，可以通过参数 -f <filter_no_third> 过滤三方仓或三方文件的统计
>
>   ```shell
>   python stats.py org -o openharmony -f true --blame
>   ```

#### 度量Repo中代码贡献量

```shell
python stats.py repo -o <organization_name> -r <repo_name> -b <ref_name> -t <ref_type> [--cloc | --blame]
```

>   使用示例
>
> 从项目代码存量维度，进行度量
>
> ```shell
> python stats.py repo -o openeuler -r hmir -b hmir-tauri -t Branch --cloc
> ```
>
> 从贡献者代码量维度，进行度量
>
> ```shell
> python stats.py repo -o openeuler -r hmir -b hmir-tauri -t Branch --blame
> ```
>
> - 如果组织是openharmony可以省略<organization_name>传参，分支是master可以省略<ref_name>和<ref_type>传参
>
>   ```shell
>   python stats.py repo -r docs --blame
>   ```
>
> - <repo_name>支持传入多个
>
>   ```shell
>   python stats.py repo -r docs community --cloc
>   ```

#### 度量File中代码贡献量

```shell
python stats.py file -o <organization_name> -r <repo_name> -b <ref_name> -t <ref_type> -p <file_path>
```

>   使用示例
>
> ```shell
> python stats.py file -o openharmony-sig -r aki -b v1.0.0 -t Tag -p aki\src\version.cpp
> ```
>
> - 如果组织是openharmony可以省略<organization_name>传参，分支是master可以省略<ref_name>和<ref_type>传参
>
>   ```shell
>   python stats.py file -r docs -p docs\README.md
>   ```
>
> - <file_path>支持传入多个
>
>   ```shell
>   python stats.py file -r docs -p docs\README.md docs\README_zh.md
>   ```

#### 度量commit中代码贡献量

```shell
python stats.py commit -o <organization_name> -r <repo_name> -s <commit_id>
```

> 使用示例
>
> ```shell
> python stats.py commit -o openharmony-sig -r aki -s 7745443ef405f0d88027609648814d8851296b75
> ```
>
> - 如果组织是openharmony省略<organization_name>传参
>
>   ```shell
>   python stats.py commit -r docs -s 54e44a392e6ccb7f39e5f7116b79dfb16a92814b
>   ```
>
> - <commit_id>支持传入多个
>
>   ```shell
>   python stats.py commit -r docs -s 54e44a392e6ccb7f39e5f7116b79dfb16a92814b f8f407e113861ea5b401d33961df6b5122f955ed
>   ```

#### 度量version中代码贡献量

version属于组织内部的界定术语，而非git原生。工具对version的统计，主定位是openharmony组织的目前主要版本。

对于openharmony未来版本，或其他组织，需参考后文进阶配置，对接口和配置文件，进行特性扩展。

```shell
python stats.py version -v <version_name> -f <filter_no_third> [--cloc | --blame | --group]
```

>   使用示例
>
>   从项目代码存量维度，进行度量
>
>   ```shell
>   python stats.py version -v OpenHarmony-1.0 --cloc
>   ```
>
>   从贡献者代码量维度，进行度量
>
>   ```shell
>   python stats.py version -v OpenHarmony-1.0 -f true --blame
>   ```
>
>   从企业各方代码量维度，进行度量
>
>   ```shell
>   python stats.py version -v OpenHarmony-1.0 --group
>   ```
>
>   - <version_name>支持传入多个
>
>     ```shell
>     python stats.py version -v OpenHarmony-1.0 OpenHarmony-2.0-Canary --group
>     ```
>
>     

目前，内置可选统计版本名如下，如需配置其他版本名，参考 进阶配置·关于openharmony版本统计的特性配置

```
OpenHarmony-1.0, OpenHarmony_release_v1.1.0, 
OpenHarmony-2.0-Canary, 
OpenHarmony-3.0-LTS, OpenHarmony-3.1-Release, OpenHarmony-3.2-Release,
OpenHarmony-4.0-Beta1,
```



### 进阶配置

#### 仓库检出与结果报告输出目录的选择

工具默认产出，在工具根目录的output文件夹中。

如果想要更换到其他目录，需配置目录信息，于工具根目录下的`config.toml`文件 

将目录信息，替换文件中的"output/"，需要保留双引号。

```toml
output_path = "output/"
```

#### 对文件或语言是否记入统计的配置

- 文件配置

  配置需要跳过的文件，于工具根目录下的`config.toml`文件 

  ```toml
  cloc_file_no_scan = ["package\\-lock\\.json$", "\\.patch$"]
  ```

  如默认配置中，会跳过 `packatge-lock.json`和 `.patch` 文件

- 语言配置

  工具对代码存量的统计，是依赖于[cloc开源工具](https://github.com/AlDanial/cloc)

  可以参考[cloc自定义识别语言官方文档](https://github.com/AlDanial/cloc#create-custom-language-definitions-)，对工具根目录下的tools/lang_definitions.txt文件予以配置

#### 关于openharmony版本统计的特性配置

- 对其他新增版本统计的配置，需配置新版本信息，于工具根目录下的`config.toml`文件 

  ```toml
  # 配置示例
  [openharmony]
  [openharmony."OpenHarmony-3.0-LTS"] # 版本名称，也是命令行工具需要传入的名字
  time = "2021-09-30" # 版本发行时间
  manifest = "default" # 版本所涉及仓库的manifest组织形式
  ref = "OpenHarmony-v3.0-LTS" # 所涉及仓的索引名称
  ref_type = "Tag" # 所涉及仓的索引类型（Branch/Tag)
  ```

  > 关于manifest的进一步说明
  >
  > openharmony组织，通过[manifest](https://gitee.com/openharmony/manifest)记录组织各版本涉及的
  >
  > 目前可配置的有三种形式：
  >
  > - "just_all_repo" ：组织下所有仓库
  >
  > - "ohos_and_chipsets"：通过ohos.xml和chipsets组织，例如[OpenHarmony-3.2-Release](https://gitee.com/openharmony/manifest/tree/OpenHarmony-v3.2-Release)
  >
  > - "default"：通过default.xml组织，例如[OpenHarmony-3.0-LTS](https://gitee.com/openharmony/manifest/tree/OpenHarmony-v3.0-LTS)

- 用于校订统计结果精度的白名单配置

  - 对于特殊的开发者邮箱，无法精准识别归属或公司的情况，可以通过工具根目录下的`tools/oh_modify_email_host.xlsx`文件予以配置
  - 对于特殊的文件，无法精准识别归属的情况，可以通过工具根目录下的tools/oh_modify_file_owner.xlsx文件予以配置

#### 关于非openharmony版本统计的特性配置

这部分配置已经需要深入源码了，可以参考后文 参与贡献 的部分

- 对于版本的特性定义，通过扩展`VersionInfoHandler`接口实现
- 对于统计的特性定义，通过扩展`ReportInfoHandler`接口实现



### 报告说明

#### 项目代码量统计报告

以OpenHarmony-3.2的结果报告为例，予以说明。其他统计对象的此类报告，皆以此为基础，作列的删减。

![image-20230630170855735](./docs/pic/README/image-20230630170855735.png)

- 仓库链接、仓库名称、ref

  仓库的基本信息，ref是仓库的索引，可能是分支名Branch、里程碑Tag名、commit名SHA

- blank、comment、code

  分别对应仓库代码的空白行数、注释行数、代码行数


- code_sum

  空白行数、注释行数、代码行数的总和

#### 贡献者代码量统计报告

以OpenHarmony-3.2的结果报告为例，予以说明。其他统计对象的此类报告，皆以此为基础，作列的删减。

![image-20230630171442343](./docs/pic/README/image-20230630171442343.png)

每一行，是贡献者在某一文件中的贡献记录

- 仓库链接、文件名称、版权

  用于定位贡献者所贡献的文件位置，与该文件所识别到的版权记录

- 贡献者邮箱、邮箱属主、公司

  用于记录贡献者的基本信息，与该贡献者被识别到的公司归属

- 贡献行数

  是该贡献者在该文件中贡献的空白行数、注释行数、代码行数的总和

#### 各方代码量统计报告

以OpenHarmony-3.2的结果报告为例，予以说明。

![image-20230630172122219](./docs/pic/README/image-20230630172122219.png)

- 时间、版本、代码仓数量

  基本版本信息，其发布时间、名称与涉及代码仓数量

- 华为代码

  华为方在发布版本中的贡献，包括空白行数、注释行数、代码行数的总和

- 共建代码

  华为共建伙伴在发布版本中的贡献，包括空白行数、注释行数、代码行数的总和

- 共建代码

  独立个人在发布版本中的贡献，包括空白行数、注释行数、代码行数的总和

- 三方代码

  其他三方在发布版本中的贡献，包括空白行数、注释行数、代码行数的总和

- 总量

  前面各方在该版本中的贡献总和，是该版本代码总量

  

### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
