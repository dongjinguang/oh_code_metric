""" 配置信息方法模块

用于读取配置文件，并为工具提供全局配置信息
"""
import logging
import os

import toml as toml

CONFIG = {}
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def init_global_config_setting():
    global CONFIG
    if not CONFIG:
        CONFIG = get_static_config()
    return CONFIG


def init_output_path():
    output_path = get_specified_config_info('output_path')
    default_output_path = get_specified_config_info('default')['output_path']
    if output_path.lower() == default_output_path:
        output_path = os.path.join('\\'.join(BASE_DIR.split('\\')[:-1]), default_output_path)
    if not os.path.exists(output_path):
        os.makedirs(output_path, mode=0o777, exist_ok=False)
    return output_path


def get_static_config():
    config_info = get_whole_config_info()
    static_config_dict = dict(
        access_token=config_info['gitee_token'],
        output_path=config_info['output_path'],
        environment=config_info['environment'],
        cloc_file_no_scan=config_info['cloc_file_no_scan'],
        default_output_path=config_info['default']['output_path'],
        tool_path=config_info['default']['tool_path'],
        file_type=config_info['default']['excel_type_suffix'],
        max_page_for_repos=config_info['default']['api_max_page_to_get_org_repos'],
        no_author=config_info['default']['no_author_email_info'],
        no_host=config_info['default']['no_email_host_info'],
        no_copyright=config_info['default']['no_copyright_info'],
        org_repos_info=config_info['default']['organization_repos_info_name'],
        repos_path=config_info['default']['repo_output_path'],
        version_repos_info=config_info['default']['version_repos_info_name'],
        host_com_file=config_info['default']['author_company_def_file'],
        cloc_path=config_info['default']['stats_cloc_output_path'],
        cloc_tool=config_info['default']['stats_cloc_tool'],
        lang_def_file=config_info['default']['stats_cloc_lang_def_file'],
        blame_path=config_info['default']['stats_blame_output_path'],
        show_path=config_info['default']['stats_show_output_path'],
        commit_lines_report=config_info['default']['report_commit_line_name'],
        coder_lines_report=config_info['default']['report_coder_line_name'],
        code_lines_report=config_info['default']['report_code_line_name'],
        group_lines_report=config_info['default']['report_group_line_name'],
    )
    return static_config_dict


def get_org_dynamic_config(org_name):
    org_config = get_specified_config_info(org_name)
    # 对于没有配置的组织，默认统计当下所有仓的master
    if not org_config:
        org_config = {'master': dict(time='now', manifest='just_all_repo', ref='master', ref_type='Branch')}
    return org_config


def get_specified_config_info(config_tag):
    config_info = get_whole_config_info()
    if config_tag in config_info:
        dynamic_config_dict = config_info[config_tag]
    else:
        logging.warning('配置信息中，没有找到' + config_tag + ', 将返回空字典')
        dynamic_config_dict = {}
    return dynamic_config_dict


def get_whole_config_info():
    config_file = os.path.join(BASE_DIR, "../config.toml")
    with open(config_file, "r", encoding="utf-8") as f:
        config_info = toml.load(f)
    return config_info


if __name__ == '__main__':
    pass
