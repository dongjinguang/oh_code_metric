### Python 实现方案和使用文档

#### 1. 项目目标

该方案的目标是从一个 Excel 文件中提取不同仓库路径下的 `Committers` 列表，过滤和去重后，按照字母顺序排序输出，并将结果同步写入到文件和打印到控制台。支持的仓库路径包括 `openharmony`、`openharmony-sig` 和 `openharmony-tpc`，且根据仓库状态（“开始”、“关闭” 或 “暂停”）来筛选数据。

#### 2. 实现步骤

##### 1. **导入依赖库**

为了处理 Excel 文件和命令行参数，首先要导入 `pandas` 和 `argparse` 库。

```
pythonCopy codeimport pandas as pd
import argparse
```

##### 2. **读取 Excel 文件**

通过 `pandas` 的 `read_excel` 函数读取用户提供的 Excel 文件。

```
pythonCopy codedef read_data(file_path):
    df = pd.read_excel(file_path)
    return df
```

##### 3. **提取 Committers**

根据仓库路径过滤并提取符合条件的 committers（仓库状态为“开始”、“关闭”或“暂停”），同时清理 committers 字段，将每个路径下的 committer 去重并存储在对应的仓库分类下。

```
pythonCopy codedef extract_committers_by_repo(df):
    valid_repos = df[df['码云仓状态'].isin(['开始', '关闭', '暂停'])]

    repo_keywords = ['openharmony', 'openharmony-sig', 'openharmony-tpc']
    repo_committers = {key: set() for key in repo_keywords}

    def extract_and_clean(committers_column, repo_type):
        for committers in committers_column:
            if pd.notna(committers):
                committers_list = [committer.strip() for committer in committers.split(',')]
                repo_committers[repo_type].update(committers_list)

    for keyword in repo_keywords:
        repos = valid_repos[valid_repos['仓路径'].str.contains(keyword)]
        extract_and_clean(repos['Committers'], keyword)

    for key in repo_committers:
        repo_committers[key] = {committer for committer in repo_committers[key] if committer.strip()}

    return repo_committers
```

##### 4. **同步输出到控制台和文件**

将输出结果同时打印到控制台，并同步写入到指定的输出文件中。

```
pythonCopy codedef output_to_console_and_file(file, text):
    print(text)  # 输出到控制台
    file.write(text + '\n')  # 输出到文件
```

##### 5. **主函数逻辑**

主函数负责调用上述函数实现逻辑，从 Excel 中提取 committers 并排序后输出到文件和控制台。

```
pythonCopy codedef main(file_path, output_file_path):
    df = read_data(file_path)
    committers_by_repo = extract_committers_by_repo(df)

    with open(output_file_path, 'w', encoding='utf-8') as file:
        for repo, committers in committers_by_repo.items():
            sorted_committers = sorted(committers)
            
            output_to_console_and_file(file, f"\n仓库路径: {repo}")
            output_to_console_and_file(file, "去重后的 Committers 清单 (按字母顺序排序):")
            for committer in sorted_committers:
                output_to_console_and_file(file, committer)
            output_to_console_and_file(file, f"去重后的 Committers 总数: {len(sorted_committers)}")
```

##### 6. **命令行参数解析**

通过 `argparse` 库解析命令行参数，允许用户从命令行中传递 Excel 文件路径和输出文件路径。

```
pythonCopy codeif __name__ == "__main__":
    parser = argparse.ArgumentParser(description="统计 committers 数据")
    parser.add_argument('file_path', type=str, help="Excel 文件路径")
    parser.add_argument('output_file', type=str, help="输出文件路径")
    args = parser.parse_args()
    main(args.file_path, args.output_file)
```

#### 3. 使用文档

##### 1. **准备工作**

确保你已安装 Python 及所需的依赖库。可以通过如下命令安装 `pandas`：

```
bash


Copy code
pip install pandas
```

##### 2. **Python 脚本保存**

将完整的脚本代码保存为 `sig_managent_committer_extract.py` 文件：

```
touch sig_managent_committer_extract.py
vim sig_managent_committer_extract.py
```

将上述代码复制并粘贴到文件中。

##### 3. **准备 Excel 数据**

确保 Excel 数据格式正确。需要包含以下字段：

- **仓路径**：存放不同仓库路径，例如 `openharmony`、`openharmony-sig`、`openharmony-tpc`。
- **Committers**：对应的 Committer 列表，以逗号分隔。
- **码云仓状态**：仓库的状态，取值为“开始”、“关闭”或“暂停”。

##### 4. **运行脚本**

在命令行中运行脚本，传入 Excel 文件路径和输出文件路径：

```
python sig_managent_committer_extract.py /path/to/repo-all.xlsx /path/to/output.txt
```

##### 5. **输出文件示例**

在 `output.txt` 文件中，您将看到类似如下的结果：

```
仓库路径: openharmony
去重后的 Committers 清单 (按字母顺序排序):
andy-telink
aqxyjay
autumn330
...
去重后的 Committers 总数: 168

仓库路径: openharmony-sig
去重后的 Committers 清单 (按字母顺序排序):
algoideas
arcticsun
biubiu2012
...
去重后的 Committers 总数: 54

仓库路径: openharmony-tpc
去重后的 Committers 清单 (按字母顺序排序):
MaDiXin
mouqx
pilipala195
...
去重后的 Committers 总数: 3
```

#### 4. 注意事项

- 确保 Excel 文件中没有空白的 Committer 名称。如果有，请手动清理或使用脚本逻辑去除空行。
- 仓库路径中的关键词 `openharmony`、`openharmony-sig`、`openharmony-tpc` 是硬编码的。如果需要支持其他仓库路径，可以在代码中相应调整。